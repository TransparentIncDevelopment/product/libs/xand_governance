use crate::models::proposal::{Proposal, ProposalStatus};
use crate::models::vote::VoteTallies;

pub trait XandProposalStatusCheck {
    fn check_status(proposal: &Proposal, tallies: &VoteTallies) -> ProposalStatus {
        if Self::meets_accept_criteria(proposal, tallies) {
            ProposalStatus::Accepted
        } else if Self::meets_reject_criteria(proposal, tallies) {
            ProposalStatus::Rejected
        } else {
            ProposalStatus::Open
        }
    }
    fn meets_accept_criteria(proposal: &Proposal, tallies: &VoteTallies) -> bool;
    fn meets_reject_criteria(proposal: &Proposal, tallies: &VoteTallies) -> bool;
}
