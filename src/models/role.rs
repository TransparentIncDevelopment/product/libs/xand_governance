#[derive(Debug, Eq, PartialEq)]
pub enum Role {
    Member,
    Validator,
    Trust,
    LimitedAgent,
}
