use crate::{
    models::proposal::Proposal,
    models::role::Role,
    permissions::permissions_engine::permission_groups::{
        limited_agent_permissions::LimitedAgentPermissions, member_permissions::MemberPermissions,
        port::Permissions, validator_permissions::ValidatorPermissions,
    },
};

mod permission_groups;

pub struct PermissionsEngine<M: Permissions, V: Permissions, L: Permissions> {
    member_perms: M,
    validator_perms: V,
    limited_agent_perms: L,
}

impl PermissionsEngine<MemberPermissions, ValidatorPermissions, LimitedAgentPermissions> {
    pub fn default() -> Self {
        Self {
            member_perms: MemberPermissions {},
            validator_perms: ValidatorPermissions {},
            limited_agent_perms: LimitedAgentPermissions {},
        }
    }
}

impl<P: Permissions, V: Permissions, L: Permissions> PermissionsEngine<P, V, L> {
    #[cfg(test)]
    pub fn new(member_perms: P, validator_perms: V, limited_agent_perms: L) -> Self {
        Self {
            member_perms,
            validator_perms,
            limited_agent_perms,
        }
    }

    pub(super) fn can_vote(&self, role: &Role, proposal: &Proposal) -> bool {
        match role {
            Role::Member => self.member_perms.can_vote(proposal),
            Role::Validator => self.validator_perms.can_vote(proposal),
            Role::Trust => false,
            Role::LimitedAgent => self.limited_agent_perms.can_vote(proposal),
        }
    }

    pub(super) fn can_propose(&self, role: &Role, proposal: &Proposal) -> bool {
        match role {
            Role::Member => self.member_perms.can_propose(proposal),
            Role::Validator => self.validator_perms.can_propose(proposal),
            Role::Trust => false,
            Role::LimitedAgent => self.limited_agent_perms.can_propose(proposal),
        }
    }
}
#[cfg(test)]
mod tests {
    use super::*;
    use crate::permissions::permissions_engine::permission_groups::port::MockPermissions;

    #[test]
    fn can_propose__calls_member_perms_once_for_member_role() {
        // Given
        let role = Role::Member;
        let proposal = Proposal::AddMember;
        let mut mock_member_perms = MockPermissions::new();
        mock_member_perms
            .expect_can_propose()
            .times(1)
            .return_const(true);
        let mock_val_perms = MockPermissions::new();
        let mock_la_perms = MockPermissions::new();
        let engine = PermissionsEngine::new(mock_member_perms, mock_val_perms, mock_la_perms);

        // When
        let _res = engine.can_propose(&role, &proposal);

        // Then mock_member_perms.can_propose() is called once and only once
    }

    #[test]
    fn can_propose__calls_validator_perms_once_for_validator_role() {
        // Given
        let role = Role::Validator;
        let proposal = Proposal::AddMember;
        let mock_member_perms = MockPermissions::new();
        let mut mock_val_perms = MockPermissions::new();
        mock_val_perms
            .expect_can_propose()
            .times(1)
            .return_const(true);

        let mock_la_perms = MockPermissions::new();
        let engine = PermissionsEngine::new(mock_member_perms, mock_val_perms, mock_la_perms);

        // When
        let _res = engine.can_propose(&role, &proposal);

        // Then mock_val_perms.can_propose() is called once and only once
    }

    #[test]
    fn can_propose__calls_limited_agent_perms_once_for_limited_agent_role() {
        // Given
        let role = Role::LimitedAgent;
        let proposal = Proposal::AddMember;
        let mock_member_perms = MockPermissions::new();
        let mock_val_perms = MockPermissions::new();

        let mut mock_la_perms = MockPermissions::new();
        mock_la_perms
            .expect_can_propose()
            .times(1)
            .return_const(true);

        let engine = PermissionsEngine::new(mock_member_perms, mock_val_perms, mock_la_perms);

        // When
        let _res = engine.can_propose(&role, &proposal);

        // Then mock_la_perms.can_propose() is called once and only once
    }

    #[test]
    fn can_vote__calls_member_perms_once_for_member_role() {
        // Given
        let role = Role::Member;
        let proposal = Proposal::AddMember;
        let mut mock_member_perms = MockPermissions::new();
        mock_member_perms
            .expect_can_vote()
            .times(1)
            .return_const(true);
        let mock_val_perms = MockPermissions::new();
        let mock_la_perms = MockPermissions::new();
        let engine = PermissionsEngine::new(mock_member_perms, mock_val_perms, mock_la_perms);

        // When
        let _res = engine.can_vote(&role, &proposal);

        // Then mock_member_perms.can_vote() is called once and only once
    }

    #[test]
    fn can_vote__calls_validator_perms_once_for_validator_role() {
        // Given
        let role = Role::Validator;
        let proposal = Proposal::AddMember;
        let mock_member_perms = MockPermissions::new();
        let mut mock_val_perms = MockPermissions::new();
        mock_val_perms.expect_can_vote().times(1).return_const(true);

        let mock_la_perms = MockPermissions::new();
        let engine = PermissionsEngine::new(mock_member_perms, mock_val_perms, mock_la_perms);

        // When
        let _res = engine.can_vote(&role, &proposal);

        // Then mock_val_perms.can_vote() is called once and only once
    }

    #[test]
    fn can_vote__calls_limited_agent_perms_once_for_limited_agent_role() {
        // Given
        let role = Role::LimitedAgent;
        let proposal = Proposal::AddMember;
        let mock_member_perms = MockPermissions::new();
        let mock_val_perms = MockPermissions::new();

        let mut mock_la_perms = MockPermissions::new();
        mock_la_perms.expect_can_vote().times(1).return_const(true);

        let engine = PermissionsEngine::new(mock_member_perms, mock_val_perms, mock_la_perms);

        // When
        let _res = engine.can_vote(&role, &proposal);

        // Then mock_la_perms.can_vote() is called once and only once
    }

    #[test]
    fn can_vote__all_proposals_for_trust_returns_false() {
        // Given
        let proposals = Proposal::all();
        let role = Role::Trust;
        let engine = PermissionsEngine::default();

        for p in proposals.iter() {
            // When
            let res = engine.can_vote(&role, p);
            assert!(!res, "Assertion failed for: {:?}", p);
        }
    }
}
