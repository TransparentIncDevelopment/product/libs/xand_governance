use crate::models::proposal::Proposal;
use crate::permissions::permissions_engine::permission_groups::port::Permissions;

pub struct ValidatorPermissions {}

impl Permissions for ValidatorPermissions {
    fn can_vote(&self, proposal: &Proposal) -> bool {
        match proposal {
            Proposal::AddMember => true,
            Proposal::RemoveMember => true,
            Proposal::AddValidator => true,
            Proposal::RemoveValidator => true,
            Proposal::SetTrust => false,
            Proposal::SetLimitedAgent => false,
            Proposal::AddCidrBlock => true,
            Proposal::RemoveCidrBlock => true,
            Proposal::SetValidatorEmissionRate => false,
            Proposal::RuntimeUpgrade => true,
        }
    }

    fn can_propose(&self, proposal: &Proposal) -> bool {
        match proposal {
            Proposal::AddMember => true,
            Proposal::RemoveMember => true,
            Proposal::AddValidator => true,
            Proposal::RemoveValidator => true,
            Proposal::SetTrust => false,
            Proposal::SetLimitedAgent => false,
            Proposal::AddCidrBlock => true,
            Proposal::RemoveCidrBlock => true,
            Proposal::SetValidatorEmissionRate => false,
            Proposal::RuntimeUpgrade => true,
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn can_vote__add_member_returns_true() {
        // Given
        let perms = ValidatorPermissions {};
        let proposal = Proposal::AddMember;

        // When
        let res = perms.can_vote(&proposal);

        // Then
        assert!(res);
    }

    #[test]
    fn can_propose__add_member_returns_true() {
        // Given
        let perms = ValidatorPermissions {};
        let proposal = Proposal::AddMember;

        // When
        let res = perms.can_propose(&proposal);

        // Then
        assert!(res);
    }

    #[test]
    fn can_vote__remove_member_returns_true() {
        // Given
        let perms = ValidatorPermissions {};
        let proposal = Proposal::RemoveMember;

        // When
        let res = perms.can_vote(&proposal);

        // Then
        assert!(res);
    }

    #[test]
    fn can_propose__remove_member_returns_true() {
        // Given
        let perms = ValidatorPermissions {};
        let proposal = Proposal::RemoveMember;

        // When
        let res = perms.can_propose(&proposal);

        // Then
        assert!(res);
    }

    #[test]
    fn can_vote__add_validator_returns_true() {
        // Given
        let perms = ValidatorPermissions {};
        let proposal = Proposal::AddValidator;

        // When
        let res = perms.can_vote(&proposal);

        // Then
        assert!(res);
    }

    #[test]
    fn can_propose__add_validator_returns_true() {
        // Given
        let perms = ValidatorPermissions {};
        let proposal = Proposal::AddValidator;

        // When
        let res = perms.can_propose(&proposal);

        // Then
        assert!(res);
    }

    #[test]
    fn can_propose__remove_validator_returns_true() {
        // Given
        let perms = ValidatorPermissions {};
        let proposal = Proposal::RemoveValidator;

        // When
        let res = perms.can_propose(&proposal);

        // Then
        assert!(res);
    }

    #[test]
    fn can_vote__remove_validator_returns_true() {
        // Given
        let perms = ValidatorPermissions {};
        let proposal = Proposal::RemoveValidator;

        // When
        let res = perms.can_vote(&proposal);

        // Then
        assert!(res);
    }

    #[test]
    fn can_vote__add_cidr_block_returns_true() {
        // Given
        let perms = ValidatorPermissions {};
        let proposal = Proposal::AddCidrBlock;

        // When
        let res = perms.can_vote(&proposal);

        // Then
        assert!(res);
    }

    #[test]
    fn can_propose__add_cidr_block_returns_true() {
        // Given
        let perms = ValidatorPermissions {};
        let proposal = Proposal::AddCidrBlock;

        // When
        let res = perms.can_propose(&proposal);

        // Then
        assert!(res);
    }

    #[test]
    fn can_vote__remove_cidr_block_returns_true() {
        // Given
        let perms = ValidatorPermissions {};
        let proposal = Proposal::RemoveCidrBlock;

        // When
        let res = perms.can_vote(&proposal);

        // Then
        assert!(res);
    }

    #[test]
    fn can_propose__remove_cidr_block_returns_true() {
        // Given
        let perms = ValidatorPermissions {};
        let proposal = Proposal::RemoveCidrBlock;

        // When
        let res = perms.can_propose(&proposal);

        // Then
        assert!(res);
    }

    #[test]
    fn can_vote__set_limited_agent_returns_false() {
        // Given
        let perms = ValidatorPermissions {};
        let proposal = Proposal::SetLimitedAgent;

        // When
        let res = perms.can_vote(&proposal);

        // Then
        assert!(!res);
    }

    #[test]
    fn can_propose__set_limited_agent_returns_false() {
        // Given
        let perms = ValidatorPermissions {};
        let proposal = Proposal::SetLimitedAgent;

        // When
        let res = perms.can_propose(&proposal);

        // Then
        assert!(!res);
    }

    #[test]
    fn can_propose__set_trust_agent_returns_false() {
        // Given
        let perms = ValidatorPermissions {};
        let proposal = Proposal::SetTrust;

        // When
        let res = perms.can_propose(&proposal);

        // Then
        assert!(!res);
    }

    #[test]
    fn can_vote__set_trust_agent_returns_false() {
        // Given
        let perms = ValidatorPermissions {};
        let proposal = Proposal::SetTrust;

        // When
        let res = perms.can_vote(&proposal);

        // Then
        assert!(!res);
    }

    #[test]
    fn can_propose__set_validator_emission_rate_returns_false() {
        // Given
        let perms = ValidatorPermissions {};
        let proposal = Proposal::SetValidatorEmissionRate;

        // When
        let res = perms.can_propose(&proposal);

        // Then
        assert!(!res);
    }

    #[test]
    fn can_vote__set_validator_emission_rate_returns_false() {
        // Given
        let perms = ValidatorPermissions {};
        let proposal = Proposal::SetValidatorEmissionRate;

        // When
        let res = perms.can_vote(&proposal);

        // Then
        assert!(!res);
    }

    #[test]
    fn can_propose__runtime_upgrade_returns_true() {
        // Given
        let perms = ValidatorPermissions {};
        let proposal = Proposal::RuntimeUpgrade;

        // When
        let res = perms.can_propose(&proposal);

        // Then
        assert!(res);
    }

    #[test]
    fn can_vote__runtime_upgrade_returns_true() {
        // Given
        let perms = ValidatorPermissions {};
        let proposal = Proposal::RuntimeUpgrade;

        // When
        let res = perms.can_vote(&proposal);

        // Then
        assert!(res);
    }
}
