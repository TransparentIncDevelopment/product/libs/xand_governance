use crate::proposal_status::models::{RequiredThreshold, VoteWeights};
use crate::{
    models::{proposal::Proposal, vote::VoteTallies},
    ports::status::XandProposalStatusCheck,
};

mod models;
#[cfg(test)]
mod tests;

pub struct XandProposalStatusCheckImpl {}

impl XandProposalStatusCheck for XandProposalStatusCheckImpl {
    fn meets_accept_criteria(proposal: &Proposal, tallies: &VoteTallies) -> bool {
        let threshold = Self::passing_vote_threshold(proposal);
        let vote_weights = Self::vote_weights_of_roles(proposal);
        Self::success_algorithm(tallies, &threshold, &vote_weights)
    }

    fn meets_reject_criteria(proposal: &Proposal, tallies: &VoteTallies) -> bool {
        let threshold = Self::passing_vote_threshold(proposal);
        let vote_weights = Self::vote_weights_of_roles(proposal);
        Self::failure_algorithm(tallies, &threshold, &vote_weights)
    }
}

impl XandProposalStatusCheckImpl {
    pub fn success_algorithm(
        tallies: &VoteTallies,
        passing_threshold: &RequiredThreshold,
        vote_weights: &VoteWeights,
    ) -> bool {
        if tallies.member_tally.total_participant_count == 0 {
            // If there are no currently registered members, validators must surpass the threshold on their own
            // This is re-written from `yea_val > (threshold_n/threshold_d)tot_val`
            let a = passing_threshold.ratio().denominator * tallies.validator_tally.yea_votes;
            let b = passing_threshold.ratio().numerator
                * tallies.validator_tally.total_participant_count;

            a > b
        } else {
            // NOTE: (yea_mem/tot_mem)(weightm_n/weightm_d) + (yea_val/tot_val)(weightv_n/weightv_d) > (threshold_n/threshold_d)
            // weighted member yea votes and weighted validator yea votes should surpass required threshold
            // Without division:
            //                      a                               +                           b                          >                 c
            // [(yea_mem*weightm_n)*(tot_val*weightv_d)*threshold_d] + [(yea_val * weightv_n)*(tot_mem*weightm_d)*thresh_d] > [threshold_n*(tot_val*weightv_d)*(tot_mem*weightm_d)]
            let a = tallies.member_tally.yea_votes
                * vote_weights.member_weight().numerator
                * tallies.validator_tally.total_participant_count
                * vote_weights.validator_weight().denominator
                * passing_threshold.ratio().denominator;
            let b = tallies.validator_tally.yea_votes
                * vote_weights.validator_weight().numerator
                * tallies.member_tally.total_participant_count
                * vote_weights.member_weight().denominator
                * passing_threshold.ratio().denominator;
            let c = passing_threshold.ratio().numerator
                * tallies.validator_tally.total_participant_count
                * vote_weights.validator_weight().denominator
                * tallies.member_tally.total_participant_count
                * vote_weights.member_weight().denominator;

            a + b > c
        }
        // 0 validators is an impossible situation - since the network would be frozen (and we do not
        // allow removal of the last validator)
    }

    pub fn failure_algorithm(
        tallies: &VoteTallies,
        passing_threshold: &RequiredThreshold,
        vote_weights: &VoteWeights,
    ) -> bool {
        if tallies.member_tally.total_participant_count == 0 {
            // If there are no currently registered members, validators must reach the threshold prevention requirement themselves
            // This is re-written from `nay_val >= (threshold_failure_n/threshold_d)tot_val`
            let a = passing_threshold.ratio().denominator * tallies.validator_tally.nay_votes;
            let b = passing_threshold.failure_numerator()
                * tallies.validator_tally.total_participant_count;

            a >= b
        } else {
            // NOTE: Inversion of `vote_success_algorithm()`
            // NOTE: (nay_mem/tot_mem)(weightm_n/weightm_d) + (nay_val/tot_val)(weightv_n/weightv_d) >= (threshold_failure_n/thresholdd)
            // Without division:
            //                      a                               +                           b                          >=                 c
            // [(nay_mem*weightm_n)*(tot_val*weightv_d)*thresholdd] + [(nay_val * weightv_n)*(tot_mem*weightm_d)*thresh_d] >= [threshold_failure_n*(tot_val*weightv_d)*(tot_mem*weightm_d)]
            let a = tallies.member_tally.nay_votes
                * vote_weights.member_weight().numerator
                * tallies.validator_tally.total_participant_count
                * vote_weights.validator_weight().denominator
                * passing_threshold.ratio().denominator;
            let b = tallies.validator_tally.nay_votes
                * vote_weights.validator_weight().numerator
                * tallies.member_tally.total_participant_count
                * vote_weights.member_weight().denominator
                * passing_threshold.ratio().denominator;
            let c = passing_threshold.failure_numerator()
                * tallies.validator_tally.total_participant_count
                * vote_weights.validator_weight().denominator
                * tallies.member_tally.total_participant_count
                * vote_weights.member_weight().denominator;

            a + b >= c
        }
        // 0 validators is an impossible situation - since the network would be frozen (and we do not
        // allow removal of the last validator)
    }
}

impl XandProposalStatusCheckImpl {
    pub fn passing_vote_threshold(proposal: &Proposal) -> RequiredThreshold {
        match proposal {
            Proposal::SetLimitedAgent => RequiredThreshold::Majority,
            Proposal::SetTrust => RequiredThreshold::Majority,
            Proposal::SetValidatorEmissionRate => RequiredThreshold::Majority,
            Proposal::AddValidator => RequiredThreshold::Supermajority,
            Proposal::RemoveValidator => RequiredThreshold::Supermajority,
            Proposal::AddMember => RequiredThreshold::Supermajority,
            Proposal::RemoveMember => RequiredThreshold::Supermajority,
            Proposal::AddCidrBlock => RequiredThreshold::Supermajority,
            Proposal::RemoveCidrBlock => RequiredThreshold::Supermajority,
            Proposal::RuntimeUpgrade => RequiredThreshold::Supermajority,
        }
    }

    pub fn vote_weights_of_roles(proposal: &Proposal) -> VoteWeights {
        match proposal {
            Proposal::SetLimitedAgent => VoteWeights::full_member_weight(),
            Proposal::SetTrust => VoteWeights::full_member_weight(),
            Proposal::SetValidatorEmissionRate => VoteWeights::full_member_weight(),
            Proposal::AddValidator => VoteWeights::split_49_51(),
            Proposal::RemoveValidator => VoteWeights::split_49_51(),
            Proposal::AddMember => VoteWeights::split_49_51(),
            Proposal::RemoveMember => VoteWeights::split_49_51(),
            Proposal::AddCidrBlock => VoteWeights::split_49_51(),
            Proposal::RemoveCidrBlock => VoteWeights::split_49_51(),
            Proposal::RuntimeUpgrade => VoteWeights::split_49_51(),
        }
    }
}
