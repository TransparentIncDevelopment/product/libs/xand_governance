use crate::models::proposal::Proposal;
use crate::models::role::Role;
use crate::permissions::permissions_engine::PermissionsEngine;
use crate::ports::rules::XandGovernancePermissions;

mod permissions_engine;

/// Responsible for working with "PermissionsEngine" and fulfilling port/trait XandGovernanceRules
pub struct XandGovernancePermissionsImpl {}

impl XandGovernancePermissions for XandGovernancePermissionsImpl {
    fn can_propose(role: Role, proposal: Proposal) -> bool {
        PermissionsEngine::default().can_propose(&role, &proposal)
    }

    fn can_vote(role: Role, proposal: Proposal) -> bool {
        PermissionsEngine::default().can_vote(&role, &proposal)
    }
}
