use crate::models::proposal::Proposal;
use crate::proposal_status::tests::templates::{
    assert_member_only_40_pct_nay_majority_remains_open,
    assert_member_only_40_pct_yea_majority_remains_open,
    assert_member_only_50_pct_nay_vote_prevents_majority_and_rejects,
    assert_member_only_majority_yea_vote_is_accepted,
    assert_member_only_majority_yea_vote_with_unexpected_validator_votes_is_accepted,
    assert_member_only_yea_votes_majority_threshold_exactly_remains_open,
};

#[test]
fn check_status__members_vote_yea_majority__accepted() {
    assert_member_only_majority_yea_vote_is_accepted(&Proposal::SetValidatorEmissionRate)
}

#[test]
fn check_status__unexpected_validator_votes_ignored_member_majority__accepted() {
    assert_member_only_majority_yea_vote_with_unexpected_validator_votes_is_accepted(
        &Proposal::SetValidatorEmissionRate,
    )
}

#[test]
fn check_status__50_percent_of_members_vote_nay__rejected() {
    assert_member_only_50_pct_nay_vote_prevents_majority_and_rejects(
        &Proposal::SetValidatorEmissionRate,
    )
}

#[test]
fn check_status__40_pct_of_members_vote_yea__open() {
    assert_member_only_40_pct_yea_majority_remains_open(&Proposal::SetValidatorEmissionRate)
}

#[test]
fn check_status__40_pct_of_members_vote_nay__open() {
    assert_member_only_40_pct_nay_majority_remains_open(&Proposal::SetValidatorEmissionRate)
}

#[test]
fn check_status__members_and_validators_vote_yea_exact_majority_threshold__open() {
    assert_member_only_yea_votes_majority_threshold_exactly_remains_open(
        &Proposal::SetValidatorEmissionRate,
    )
}
